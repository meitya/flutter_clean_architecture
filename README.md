# App Template

A Flutter base app template that implements Reso Coder's clean architecture and test driven development with BLoC state management.

-----

## Folder Structure

- Build your app's feature(s) on the **features** folder

  Each feature folder should have the 3 layers of clean architecture folder:

  - **presentation** folder

  On the presentation folder, there are folders for **bloc** which is for the BLoC state management, for **pages** which is the screen/display of this feature on the app, and the **widgets** which is for custom or complex widgets that need to get out of a page to make the page more readable

  - **domain** folder

  On the domain folder, there are folders for **entities**, **usecases** on which all the business logic resides, and **repositories** which is an abstract class that will be implemented by the another **repositories** folder on the data folder

  - **data** folder

  On the data folder, there are folders for **datasources**, **models**, and **repositories** which implements the contract stated in the domain folder.

- When a feature doesn't belong to a specific page, such as input checking which should be shared between all pages that deals with input, then put that feature on the **core** folder.

- The **test** folder structure follows the same convention as the production code structure. For the number_trivia feature for example, the **test** folder follow this structure:
  
  - core
  - features
    - number_trivia
      - data
      - domain
        - usecases
          - `get_concrete_number_trivia_test.dart`
      - presentation

-----

## Test Driven Development (TDD)

1. Make a test file (on Dart, the naming convention is usually xxx_test.dart), and make a **mock** class on the test file

    e.g:

    On `get_concrete_number_trivia_test.dart` create:

    ```dart
    class MockNumberTriviaRepository extends Mock implements NumberTriviaRepository {}
    ```

2. Make a `main()` method in which there's a variable initialization and `setUp()` method that contains the variable instatiation.

    e.g:

    ```dart
    void main() {
      GetConcreteNumberTrivia usecase;
      MockNumberTriviaRepository mockNumberTriviaRepository;

      setUp(() {
        mockNumberTriviaRepository = MockNumberTriviaRepository();
        usecase = GetConcreteNumberTrivia(mockNumberTriviaRepository);
      });
    }
    ```

    There sould be an **error** and that's expected. This is the **Red** phase of the **Red-Green-Refactor** of TDD.

3. To arrive at the **Green** phase, implement all that needs to be implemented

    e.g:
    - Create `get_concrete_number_trivia.dart` which contains:

      ```dart
      import '../repositories/number_trivia_repositories.dart';

      class GetConcreteNumberTrivia {
        final NumberTriviaRepository repository;

        GetConcreteNumberTrivia(this.repository);
      }
      ```

    - Create some tests
    - Run the tests to truly get the error in the **Red** phase 
    - Create the implementation of `execute()` `of the usecase to pass the tests you just created
    - Run the test and if it passes, then **Green** phase is achieved

-----

## Domain Layer Refactoring

